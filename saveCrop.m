[filename1, pathname1] = uigetfile({'*.tif';'*.bmp';'*.gif';'*.*'}, 'Pick an Image File');
full=imread([pathname1, filename1]);
a=320;
b=320;
c=50;
d=50;
smallSubImage = imcrop(full, [a, b, c, d]);
as=int2str(a);
bs=int2str(b);
cs=int2str(c);
ds=int2str(d);

imwrite(smallSubImage,strcat('Sub',as,',', bs,',', cs,',', ds,'.tiff'));