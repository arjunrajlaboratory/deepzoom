function [ output_args ] = tlDisplay( lvl, xlow, xhigh, ylow, yhigh, nm )
%tlDisplay Display specified tiles from specified level
%   Detailed explanation goes here
lvlS=int2str(lvl);
par=strcat(nm,'Splits');
lvl_fl = dir(strcat(par,'/*',lvlS,'_*'));
fl_nm=lvl_fl.name;

tlI = [];
for a = ylow:yhigh
    tiledI = [];
    for b = xlow:xhigh
        tl_nm=strcat(int2str(a),'x',int2str(b), '.tiff');
        path=strcat(par,'/',fl_nm,'/',tl_nm);
        img=imread(path);
        tiledI=[tiledI, img];
    end
    tlI = [tlI; tiledI];
end
imshow(tlI);

C = tlI;

end

