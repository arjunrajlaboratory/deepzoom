function C = splitIm( img, col, row )
sz=size(img);
h=sz(1);
w=sz(2);
[a, b]=size(sz);
width=floor(w/col);
height=floor(h/row);

Ax=prepSet(w, width);
Ay=prepSet(h, height);
if b==3
    C = mat2cell(img, Ay, Ax, sz(3));
else
    C = mat2cell(img, Ay, Ax);
end
end

