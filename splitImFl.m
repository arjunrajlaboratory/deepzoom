function C = splitImFl( img, col, row, res, lvl, nm)
%splitImFl Splits image into tiles of col x row
%   Makes a new file with tiles of col x row not necessarily of equal size
C=splitIm( img, col, row );


[x, y]=size (C);
par=strcat(nm,'Splits');%par='Splits';
mkdir(par)
s1=strcat(int2str(lvl), '_',int2str(x));
s2='x';
s3=int2str(y);
s4='tiles';
tl_dir=strcat(s1, s2, s3, s4);
mkdir(par, tl_dir);

for a = 1:x
    for b = 1:y
        s1=strcat(par,'/', tl_dir, '/');
        s2=int2str(a);
        s3='x';
        s4=int2str(b);
        s5='.tiff';
        tl_nm=strcat(s1, s2, s3, s4, s5);
        imwrite(C{a,b}, tl_nm, 'Resolution', res);
    end
end
end

