function C  = multSplitFl( ar, img ,nm)
%multSplitFl Take in array, split into that many parts
%   array formatted as row, col, row, col, etc. Saves files as tiff of
%   varying resolution
[~, b]=size(ar);
i=b/2;

for a=(1:i)
    in1=2*(a-1)+1;
    in2=2*(a-1)+2;
    col=ar(in1);
    row=ar(in2);
    res = 100/i*a;
    splitImFl(img, col, row, res, a, nm);
end

end

