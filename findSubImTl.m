% Select Im
[filename, pathname] = uigetfile({'*.tif';'*.bmp';'*.gif';'*.*'}, 'Pick an Image File');
rgbImage = imread([pathname, filename]);
% Get the dimensions of the image.  numberOfColorBands should be = 3.
[rows, columns, numberOfColorBands] = size(rgbImage);
% Display the original color image.
subplot(2, 2, 1);
imshow(rgbImage, []);
axis on;
caption = sprintf('Original Color Image, %d rows by %d columns.', rows, columns);
title(caption);
% Enlarge figure to full screen.
set(gcf, 'units','normalized','outerposition',[0, 0, 1, 1]);

% Select SubIm
[filename1, pathname1] = uigetfile({'*.tif';'*.bmp';'*.gif';'*.*'}, 'Pick an Image File');
smallSubImageOr=imread([pathname1, filename1]);
smallSubImage = imresize(smallSubImageOr,0.6)

% Get the dimensions of the image.  numberOfColorBands should be = 3.
[rows, columns, numberOfColorBands] = size(smallSubImage);
subplot(2, 2, 2);
imshow(smallSubImage, []);
axis on;
caption = sprintf('Template Image to Search For, %d rows by %d columns.', rows, columns);
title(caption);

templateWidth = columns;
templateHeight = rows;

% Use Channel 1 Here
% We could theoretically use all color channels and average the results
% We'll have to see what works better
channelToCorrelate = 1;  % Use Channel 1
correlationOutput = normxcorr2(smallSubImage(:,:,1), rgbImage(:,:, channelToCorrelate));
subplot(2, 2, 3);
imshow(correlationOutput, []);
axis on;
% Get the dimensions of the image.  numberOfColorBands should be = 1.
[rows, columns, numberOfColorBands] = size(correlationOutput);
caption = sprintf('Normalized Cross Correlation Output, %d rows by %d columns.', rows, columns);
title(caption);

% Find out where the normalized cross correlation image is brightest.
[maxCorrValue, maxIndex] = max(abs(correlationOutput(:)));
[yPeak, xPeak] = ind2sub(size(correlationOutput),maxIndex(1))
% Because cross correlation increases the size of the image, 
% we need to shift back to find out where it would be in the original image.
corr_offset = [(xPeak-size(smallSubImage,2)) (yPeak-size(smallSubImage,1))]

% Plot it over the original image.
subplot(2, 2, 4); % Re-display image in lower right.
imshow(rgbImage);
axis on; % Show tick marks giving pixels
hold on; % Don't allow rectangle to blow away image.
% Calculate the rectangle for the template box.  Rect = [xLeft, yTop, widthInColumns, heightInRows]
boxRect = [corr_offset(1) corr_offset(2) templateWidth, templateHeight]
% Plot the box over the image.
rectangle('position', boxRect, 'edgecolor', 'g', 'linewidth',2);
% Give a caption above the image.
title('Template Image Found in Original Image');

hfig = figure;
hax_new = copyobj(hax, hfig);
set(hax_new, 'Position', get(0, 'DefaultAxesPosition'));
%print(hfig);

Newstring = filename(1:end-4);
Newstring1 = filename1(1:end-4);


nm=strcat(Newstring,Newstring1, 'found')
%%%%%%%
rez=1200; %resolution (dpi) of final graphic
f=hfig; %f is the handle of the figure you want to export
figpos=getpixelposition(f); %dont need to change anything here
resolution=get(0,'ScreenPixelsPerInch'); %dont need to change anything here
set(f,'paperunits','inches','papersize',figpos(3:4)/resolution,'paperposition',[0 0 figpos(3:4)/resolution]); %dont need to change anything here

print(hfig,fullfile(pathname1,nm),'-dpng',['-r',num2str(rez)],'-opengl') %save file 
%%%%%%%%
uiwait(helpdlg('Done with demo!'));