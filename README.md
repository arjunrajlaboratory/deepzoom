# README #

Functions to split images into tiles and pull up various sized "patches" of the tiles to display.

multiSplitFl splits takes in an array with pairs of dimensions, makes a directory for each pair, and puts the tiled images of those dimensions into the directory. Larger tiles are saved at lower resolution.

tlDisplay pulls up "patches" of tiles (dimensions based on user input) from the input "level" (essentially magnification).

The interface is still in progress.


All the files currently uploaded are needed to run multiSplitFl and tlDisplay.
(Specifically multiSplitFl calls splitImFl and splitIm.)

The test files currently input the 'board' file. Any image file put into the same folder should also work.