% Select Im
[filename, pathname] = uigetfile({'*.tif';'*.bmp';'*.gif';'*.*'}, 'Pick an Image File');
rgbImage = imread([pathname, filename]);
rgbImage = imadjust(rgbImage);
[y1,x1]=size(rgbImage);
% Get the dimensions of the image.  numberOfColorBands should be = 3.
[rows, columns, numberOfColorBands] = size(rgbImage);
% Display the original color image.
subplot(2, 2, 1);
imshow(rgbImage, []);
axis on;
caption = sprintf('Original Color Image, %d rows by %d columns.', rows, columns);
title(caption);
% Enlarge figure to full screen.
set(gcf, 'units','normalized','outerposition',[0, 0, 1, 1]);

% Select SubIm
[filename1, pathname1] = uigetfile({'*.tif';'*.bmp';'*.gif';'*.*'}, 'Pick an Image File');
osmallSubImage=imread([pathname1, filename1]);
smallSubImage = imresize(osmallSubImage,0.6);
adsmallSubImage = imadjust(smallSubImage);
%%
sz=size(adsmallSubImage);
h = fspecial('log', sz);
smallSubImage = imfilter(adsmallSubImage,h,'replicate');
%%


% Get the dimensions of the image.  numberOfColorBands should be = 3.
[rows, columns, numberOfColorBands] = size(smallSubImage);
subplot(2, 2, 2);
imshow(adsmallSubImage, []);
axis on;
caption = sprintf('Template Image to Search For, %d rows by %d columns.', rows, columns);
title(caption);

templateWidth = columns;
templateHeight = rows;

correlationOutput = normxcorr2(smallSubImage, rgbImage);
subplot(2, 2, 3);
imshow(correlationOutput, []);
axis on;
% Get the dimensions of the image.  numberOfColorBands should be = 1.
[rows, columns, numberOfColorBands] = size(correlationOutput);
caption = sprintf('Normalized Cross Correlation Output, %d rows by %d columns.', rows, columns);
title(caption);

% Find out where the normalized cross correlation image is brightest.
[maxCorrValue, maxIndex] = max(abs(correlationOutput(:)));
[yPeak, xPeak] = ind2sub(size(correlationOutput),maxIndex(1))
% Because cross correlation increases the size of the image, 
% we need to shift back to find out where it would be in the original image.
corr_offset = [(xPeak-size(smallSubImage,2)) (yPeak-size(smallSubImage,1))]

% Plot it over the original image.
subplot(2, 2, 4); % Re-display image in lower right.
imshow(rgbImage);
axis on; % Show tick marks giving pixels
hold on; % Don't allow rectangle to blow away image.
% Calculate the rectangle for the template box.  Rect = [xLeft, yTop, widthInColumns, heightInRows]
boxRect = [corr_offset(1) corr_offset(2) templateWidth, templateHeight];
% Plot the box over the image.
rectangle('position', boxRect, 'edgecolor', 'g', 'linewidth',2);

percBoxRect=[corr_offset(1)/x1, corr_offset(2)/y1, templateWidth/x1, templateHeight/y1];
% Give a caption above the image.
title('Template Image Found in Original Image');



uiwait(helpdlg('Done with demo!'));

nm='Fused';%filename
area = input('Input the 100x tile area','s');%'1';
par=strcat(nm,'Splits');
subDir='box';
mkdir(par, subDir);
s1=strcat(par,'/', subDir, '/');
nm=strcat(s1, 'rect', area);
save(nm,'percBoxRect');